window.$ = window.jQuery = require('jquery')
const path = require('path')
const {Rol} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria para cargar archivos json
//npm install jsonfile
var jsonfile = require('jsonfile')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let rolAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado
let opciones//Almacena nivel de acceso por opciones

util.enterNavigation();//Usar enter para navegar el form

cargarRolesArchivoJson()

$('#checkAll').change(function() {
  $(':checkbox').prop('checked', $(this).is(':checked'))
})

$('#b_guardar').on('click', function () {
  $('#f_rol').submit()
})

//Manejo de evento submit del formulario
$('#f_rol').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_rol')
    var data = $('#f_rol').serializeJSON({checkboxUncheckedValue: "false"})
    data.accesos = getNivelesAcceso()
    if(!rolAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_rol tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desea eliminar el registro?')) {
      eliminar()
  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  rolAttr = null
  $("#f_rol")[0].reset()//limpia el formulario
  $('#checkAll').prop('checked',false)
  cargarRolesArchivoJson()//Restarura tabla de opciones
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  Rol.sync().then(() => {
    return Rol.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("rol","Nuevo rol #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
     console.log(err)
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  return rolAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("rol","Modificacion de rol #"+rolAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
     console.log(err)
    alert(err.errors[0].message)
  })
}

function eliminar(){
  rolAttr.destroy().then(function () {
    auditoria.insertarAuditoria("rol","Eliminacion de rol #"+rolAttr.id)
    restauraValores()
    filtrar()
  })
}

function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let roles=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Rol.findAll({
      attributes:['id','descripcion'],
      where: { $or:condiciones }
    }).then(results => {
      results.forEach(function (rol) {
        roles.push(rol.dataValues)
      });
      util.buildTable(roles, 't_rol')
    })
  }else{
    util.buildTable([], 't_rol')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Rol.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}
function comprobarPermisos(accesos){
   for (var i = 0; i < opciones.length; i++) {
      var result = $.grep(accesos, function(e){ return e.id == opciones[i].id; });
      if(result.length>0) opciones[i].tiene_acceso = result[0].tiene_acceso
   }
   return opciones
}
function prepararParaEdicion(result){
  rolAttr = result
  opciones = comprobarPermisos(result.accesos)
  util.cargarFormulario(result.dataValues,"f_rol")
  util.buildTable(checkNivelesAcceso(opciones), 't_opciones')
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}

function cargarRolesArchivoJson(){
  var file = path.join(__dirname, '../extra/modules.json')
  jsonfile.readFile(file, function(err, result) {
    opciones = JSON.parse(JSON.stringify(result))
    util.buildTable(checkNivelesAcceso(result), 't_opciones')
  })
}

function checkNivelesAcceso(opciones) {
  let opciones_acc = [];
  let todo = true
  opciones.forEach(function (item){
    if(todo){
      todo = item.tiene_acceso
    }
    item.acceso = '<input type="checkbox" id="'+item.id+'" '
    +((item.tiene_acceso)?'checked':'')+' />'
    opciones_acc.push(item)
  })
  $('#checkAll').prop('checked',todo)
  return opciones_acc
}
function getNivelesAcceso() {
  let niveles_acceso = []
  for (i = 0; i < opciones.length; i++) {
     niveles_acceso.push({
        id:opciones[i].id,
        tiene_acceso:$('#'+opciones[i].id).is(":checked")
     })
  }
  return niveles_acceso
}
