window.$ = window.jQuery = require('jquery')
const {Departamento} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let departamentoAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado

util.enterNavigation();//Usar enter para navegar el form

//Manejo de evento submit del formulario
$('#f_departamento').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_departamento')
    var data = $('#f_departamento').serializeJSON({checkboxUncheckedValue: "false"})
    console.log(data);
    if(!departamentoAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_departamento tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desa eliminar el registro?')) {
      eliminar()
  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  departamentoAttr = null
  $("#f_departamento")[0].reset()//limpia el formulario
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  Departamento.sync().then(() => {
    return Departamento.create(data)
  }).then(data =>{
    auditoria.insertarAuditoria("departamento","Nuevo departamento #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  return departamentoAttr.updateAttributes(data)
  .then(function() {
      auditoria.insertarAuditoria("departamento","Modificacion de departamento #"+departamentoAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  departamentoAttr.destroy().then(function(){
    auditoria.insertarAuditoria("departamento","Eliminacion de departamento #"+departamentoAttr.id)
    restauraValores()
    filtrar()
  })
}
function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let departamentos=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Departamento.findAll({
      attributes:['id','descripcion'],
      where: { $or:condiciones }
    }).then(results => {
      results.forEach(function (departamento) {
        departamentos.push(departamento.dataValues)
      });
      util.buildTable(departamentos, 't_departamento')
    })
  }else{
    util.buildTable([], 't_departamento')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Departamento.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  departamentoAttr = result
  util.cargarFormulario(result.dataValues,"f_departamento")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
