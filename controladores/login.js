window.$ = window.jQuery = require('jquery')
const path = require('path')
const {Usuario,Rol} = require('../modelo/modelos.js')//Modelos
/**
 * Para cifrar la contraseña del usuario.
 * @doc {https://github.com/davidwood/node-password-hash}
 * @repo {npm install password-hash --save}
 */
const phash = require('password-hash')
/**
 * Libreria para validar formularios
 * @doc {http://1000hz.github.io/bootstrap-validator/}
 * @repo {npm install bootstrap-validator}
 */
require('bootstrap-validator')

/**************************/
//Libreria para cargar archivos json
//npm install jsonfile
var jsonfile = require('jsonfile')

/**************************/
//Constantes recuperadas del main
const setUsuario = require('electron').remote.getGlobal('setUsuario') 
const createWindow = require('electron').remote.getGlobal('createWindow') //Para crear la ventana principal al logearse
const crearMenu = require('electron').remote.getGlobal('crearMenu') //Para crear el menu

/** Atributos */
let usuario, pass, opciones

/** Evento submit del form.
* LLama al metodo para recuperar el usuario y el pass que ingresa el usuario */
$('#f_login').validator().submit(function(e) {
   if (!e.isDefaultPrevented()) {
      e.preventDefault()
      recuperaUserPass()
   }
})

/**
 * Recupera y almacera los datos del usuario
 * @return void
 */
function recuperaUserPass(){
   usuario = $('#usuario').val()
   pass = $('#pass').val()
   compruebaPass(pass,usuario)
}

/**
 * Comprueba la existencia del usuario y la coincidencia del password ingresado con el almacenado en la db.
 * Si existe el usuario y coincide el password, se crea la ventana principal y el menu segun los privilegios del usuario
 * @param  {string} pass password
 * @param  {string} user usuario
 * @return {void}
 */
function compruebaPass(pass, user){
   buscarUsuario(user).then((usuario)=>{
      if (phash.verify(pass,usuario.pass)) {
         setUsuario(JSON.stringify(usuario.dataValues))
         crearMenu(comprobarPermisos(usuario.role.accesos))
         createWindow()
         window.close()
      } else {
         alert("Nombre de usuario o contraseña incorrectos")
      }
   }).catch((err)=>{
      console.error(err)
      alert("Nombre de usuario o contraseña incorrectos")
   })
}

/**
 * Busca el usuario ingresado en la db y recupera sus datos
 * @param  {string} user usuario
 * @return {[void]}
 */
function buscarUsuario(user){
   return new Promise((resolve,reject)=>{
      Usuario.findOne({
      where: { nick:{$eq:user.toUpperCase()} },
      attributes:['id','nick','pass'],
      include:[{
         model: Rol,
         attributes: ['id','accesos']
      }]
   }).then( usuario => {
         if (usuario) {
            resolve(usuario)
         } else {
            reject()
         }
      })
   })
}

/**
 * Comprueba los privilegios del usuario y los almacena en la variable en opciones
 * @param  {json} accesos Los accesos recuperados de la db
 * @return {array}        Array con los permisos del usuario
 */
function comprobarPermisos(accesos){
   for (var i = 0; i < opciones.length; i++) {
      var result = $.grep(accesos, function(e){ return e.id == opciones[i].id; });
      if(result.length>0) opciones[i].tiene_acceso = result[0].tiene_acceso
   }
   return opciones
}

/**
 * Recupera los modulos del sistema que estan almacenados en el archivo json
 * @return {void}
 */
function recuperarRoles(){
  var file = path.join(__dirname, '../extra/modules.json')
  jsonfile.readFile(file, function(err, result) {
     console.log(err);
    opciones = JSON.parse(JSON.stringify(result))
  })
}

/*Ejecuta la funcion del mismo nombre*/
recuperarRoles()

/**
 * Funcion que recupera los modulos del sistema y asigna todos los privilegios al usuario root
 * @return {void} [description]
 */
function recuperarRolesYCrearROOT(){
  let opciones = []
  var file = 'extra/modules.json'
  jsonfile.readFile(file, function(err, result) {
    opciones = JSON.parse(JSON.stringify(result))
    for (var i = 0; i < opciones.length; i++) {
       opciones[i].tiene_acceso = true
    }
    insertar_super_rol(opciones)
  })
}

/**
 * Genera un nuevo array solo los los permisos, para ser almacenado en la db
 * @param  {array} opciones modulos del sistema y permisos
 * @return {array}   permisos del root
 */
function getNivelesAcceso(opciones) {
  let niveles_acceso = []
  for (i = 0; i < opciones.length; i++) {
     niveles_acceso.push({
        id:opciones[i].id,
        tiene_acceso:opciones[i].tiene_acceso
     })
  }
  return niveles_acceso
}

/**
 * Guarda los privilegios del usuario root en la db
 * @param  {array} opciones permisos asignados al root
 * @return {void}
 */
function insertar_super_rol(opciones){
   let data = {
      descripcion:'SUPER',
      accesos: getNivelesAcceso(opciones)
   }
   Rol.sync().then(() => {
       return Rol.create(data)
   }).then(function(rol) {
      insertar_root(rol)
   }, function(err) {
     console.log(err)
   })
}

/**
 * Guarda el usuario root en la db
 * @param  {rol} rol contiene el id del rol
 * @return {void}
 */
function insertar_root(rol){
   let data = {
      nick: 'ROOT',
      pass: phash.generate('toor', {algorithm:'sha256'}),
      roleId: rol.id,
   }
  Usuario.sync().then(() => {
    return Usuario.create(data)
  }).then(function() {
  }, function(err) {
    alert(err.errors[0].message)
  })
}

/**
 * Verifica si existe o no el usuario root en la db.
 * Si no existe se crea el rol y se almacenan los datos del root en la db
 * @return {[type]} [description]
 */
function checkRoot(){
   buscarUsuario('ROOT').then().catch(()=>{
      recuperarRolesYCrearROOT()
   })
}

/*Ejecuta la funcion al iniciar la ventana login*/
checkRoot()
