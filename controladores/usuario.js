window.$ = window.jQuery = require('jquery')
const {Usuario, Rol} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**
 * Para autocompletado con lista de opciones
 * @doc {https://github.com/bassjobsen/Bootstrap-3-Typeahead}
 * @repo {npm i bootstrap-3-typeahead}
 */
const typeahead = require('bootstrap-3-typeahead');

/**
 * Para cifrar la contraseña del usuario.
 * @doc {https://github.com/davidwood/node-password-hash}
 * @repo {npm install password-hash --save}
 */
const phash = require('password-hash')


/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

/** Atributos que se usan para los buscadores  */
let fncnrs = []

let usuarioAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado

util.enterNavigation();//Usar enter para navegar el form

cargarRoles()

//Manejo de evento submit del formulario
$('#f_usuario').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_usuario')
    var data = $('#f_usuario').serializeJSON({checkboxUncheckedValue: "false"})
    if (data.pass) {
      data.pass = phash.generate(data.pass, {algorithm:'sha256'}) //cifra la contraseña con algoritmo sha256
    }
    if(!usuarioAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_usuario tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desea eliminar el registro?')) {
      eliminar()
  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  usuarioAttr = null
  $("input[type=hidden]").val("");
  $("#f_usuario")[0].reset()//limpia el formulario
  activarCampos(false)
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  Usuario.sync().then(() => {
    return Usuario.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("usuario","Nuevo usuario #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  return usuarioAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("usuario","Modificacion de usuario #"+usuarioAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  usuarioAttr.destroy().then(function () {
    auditoria.insertarAuditoria("usuario","Eliminacion de usuario #"+usuarioAttr.id)
    restauraValores()
    filtrar()
  })
}

function cargarRoles() {
   let roles = []
   Rol.findAll().then(results => {
      results.forEach(function (rol) {
         roles.push(rol.dataValues)
      });
      util.buildSelect(roles, 's_rol')
   })
}

function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let usuarios=[]
  if(filtro){
    let condiciones = {
                        nick: {$like: '%' + filtro.toUpperCase() + '%'},
                        estado: {$like: '%' + filtro + '%'}
                      }
    Usuario.findAll({
      attributes:['id','nick','estado'],
      where: { $or:condiciones, nick:{$ne:"ROOT"}},
    }).then(results => {
      results.forEach(function (u) {
        usuarios.push({
           id: u.id,
           nick: u.nick,
           estado: u.estado
        })
      });
      util.buildTable(usuarios, 't_usuario')
    })
  }else{
    util.buildTable([], 't_usuario')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Usuario.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  usuarioAttr = result
  util.cargarFormulario(result.dataValues,"f_usuario")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
  activarCampos(true)
}

/** Activa y desactiva la campos */
function activarCampos(x) {
   // Habilita o deshabilita los campos
   $('#pass').attr('disabled', x)

   // Activa o desactiva el boton para cambio de contraseña
   if (x) {
      $('#b_pass').removeAttr('hidden')
   } else {
      $('#b_pass').attr('hidden','hidden')
   }
}

$('#b_camb_cont').on('click',function() {
   $('#pass').attr('disabled', false)
   $('#pass').val('')
   $('#pass').focus()
});
