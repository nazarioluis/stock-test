window.$ = window.jQuery = require('jquery')
const {Categoria} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let categoriaAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado

util.enterNavigation();//Usar enter para navegar el form

//Manejo de evento submit del formulario
$('#f_categoria').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_categoria')
    var data = $('#f_categoria').serializeJSON({checkboxUncheckedValue: "false"})
    if(!categoriaAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_categoria tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desa eliminar el registro?')) {
      eliminar()
  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  categoriaAttr = null
  $("#f_categoria")[0].reset()//limpia el formulario
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  Categoria.sync().then(() => {
    return Categoria.create(data)
  }).then(data =>{
    auditoria.insertarAuditoria("categoria","Nueva categoria #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  return categoriaAttr.updateAttributes(data)
  .then(function() {
      auditoria.insertarAuditoria("categoria","Modificacion de categoria #"+categoriaAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  categoriaAttr.destroy().then(function(){
  auditoria.insertarAuditoria("categoria","Eliminacion de categoria #"+categoriaAttr.id)
  restauraValores()
  filtrar()
})
}
function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let categorias=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Categoria.findAll({
      attributes:['id','descripcion'],
      where: { $or:condiciones }
    }).then(results => {
      results.forEach(function (categoria) {
        categorias.push(categoria.dataValues)
      });
      util.buildTable(categorias, 't_categoria')
    })
  }else{
    util.buildTable([], 't_categoria')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Categoria.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  categoriaAttr = result
  util.cargarFormulario(result.dataValues,"f_categoria")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
