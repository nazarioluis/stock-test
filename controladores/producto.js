window.$ = window.jQuery = require('jquery')
const {Producto,Categoria} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')//Auditoria

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let productoAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado
let categorias

util.enterNavigation();//Usar enter para navegar el form

cargarCategoria()

//Manejo de evento submit del formulario
$('#f_producto').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_producto')
    var data = $('#f_producto').serializeJSON({checkboxUncheckedValue: "false"})
    if(!productoAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_producto tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desea eliminar el registro?')) {
      eliminar()
  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  ciudadAttr = null
  $("#f_producto")[0].reset()//limpia el formulario
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  if (data.categoriaId === "") data.categoriaId= null
  Producto.sync().then(() => {
    return Producto.create(data)
  }).then(data => {
    auditoria.insertarAuditoria("producto","Nuevo preoducto #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  if (data.categoriaId === "") data.categoriaId= null
  return productoAttr.updateAttributes(data)
  .then(function() {
    auditoria.insertarAuditoria("producto","Modificacion del producto #"+productoAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  productoAttr.destroy().then(function(){
  auditoria.insertarAuditoria("producto","Eliminacion del producto #"+productoAttr.id)
  restauraValores()
  filtrar()
  })
}

function cargarCategoria(){
  categorias=[{"id":"","descripcion":"SELECCIONAR CATEGORIA"}]
  Categoria.findAll().then(results => {
    results.forEach(function (categoria) {
      categorias.push(categoria.dataValues)
    });
    util.buildSelect(categorias, 's_categoria')
  })
}

function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let productos=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Producto.findAll({
      attributes:['id','descripcion','preciocompra','preciounitario','stockreal'],
      where: { $or:condiciones },
      include: [{model:Categoria,attributes:['descripcion']}]
    }).then(results => {
      results.forEach(function (preoducto) {
        productos.push({
          'id' : preoducto.id,
          'descripcion' : preoducto.descripcion,
          'preciocompra' : preoducto.preciocompra,
          'preciounitario' : preoducto.preciounitario,
          'categoria' : (preoducto.categoria)? preoducto.categoria.descripcion:""
        })
      });
      util.buildTable(productos, 't_producto')
    })
  }else{
    util.buildTable([], 't_producto')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Producto.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  productoAttr = result
  util.cargarFormulario(result.dataValues,"f_producto")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
