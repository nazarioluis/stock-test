window.$ = window.jQuery = require('jquery')
const {Seccion} = require('../modelo/modelos.js')//Modelos
const util = require('../extra/util')//Utilidades
const auditoria = require('./auditoria')

/**************************/
//Libreria css http://getbootstrap.com/css/
//npm install bootstrap
require('bootstrap')
/**************************/

/**************************/
//Libreria para validar formularios http://1000hz.github.io/bootstrap-validator/
//npm install bootstrap-validator
require('bootstrap-validator')
/**************************/

let seccionAttr//Atributo utilizado para almacenar la instacia del objeto a ser modificado

util.enterNavigation();//Usar enter para navegar el form

//Manejo de evento submit del formulario
$('#f_seccion').validator().submit(function( event ) {
  if (!event.isDefaultPrevented()) {
    util.mayus('f_seccion')
    var data = $('#f_seccion').serializeJSON({checkboxUncheckedValue: "false"})
    console.log(data);
    if(!seccionAttr){
      insertar(data)
    }else{
      modificar(data)
    }
    event.preventDefault()
  }
})

//Recupera el id del objeto al seleccionar una fila de la tabla
$('#t_seccion tbody').on('click', 'tr', function () {
    let id = ($(this).find('td').html())
    recuperarPorId(id)
})

//Restaura vista original
$('#b_cancelar').on('click', restauraValores )

$('#b_eliminar').on('click', function () {
  if (confirm('Esta seguro que desa eliminar el registro?')) {
      eliminar()
  }
})

//Restaura vista original
$('#buscador').keyup( function() {
  util.delay(function(){
    filtrar()
  }, 500 )
})

//Restarurar valores por defecto
function restauraValores(){
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Guardar')
  $('#b_cancelar').hide()
  $('#b_eliminar').hide()
  seccionAttr = null
  $("#f_seccion")[0].reset()//limpia el formulario
}

//Inserta los datos en la tabla de la base de datos
function insertar(data){
  Seccion.sync().then(() => {
    return Seccion.create(data)
  }).then(data =>{
    auditoria.insertarAuditoria("seccion","Nuevo seccion #"+data.id)
    restauraValores()
    filtrar()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

//Modificar los datos en la tabla de la base de datos
function modificar(data){
  return seccionAttr.updateAttributes(data)
  .then(function() {
      auditoria.insertarAuditoria("seccion","Modificacion de seccion #"+seccionAttr.id)
    filtrar()
    restauraValores()
  }, function(err) {
    alert(err.errors[0].message)
  })
}

function eliminar(){
  seccionAttr.destroy().then(function(){
    auditoria.insertarAuditoria("seccion","Eliminacion de seccion #"+seccionAttr.id)
    restauraValores()
    filtrar()
  })
}
function filtrar(){
  let filtro = $.trim($('#buscador').val())
  let seccions=[]
  if(filtro){
    let condiciones = {
                        descripcion: {$like: '%' + filtro.toUpperCase() + '%'},
                      }
    Seccion.findAll({
      attributes:['id','descripcion'],
      where: { $or:condiciones }
    }).then(results => {
      results.forEach(function (seccion) {
        seccions.push(seccion.dataValues)
      });
      util.buildTable(seccions, 't_seccion')
    })
  }else{
    util.buildTable([], 't_seccion')
  }
}

//Se recupera por id y se prepara para edicion o eliminacion
function recuperarPorId(id){
  Seccion.findById(id).then(result => {
    prepararParaEdicion(result)
  })
}

function prepararParaEdicion(result){
  restauraValores()
  seccionAttr = result
  util.cargarFormulario(result.dataValues,"f_seccion")
  $('a[href="#formulario"]').tab('show');
  $('#b_guardar').html('<i class="glyphicon glyphicon-ok-circle"></i> Actualizar');
  $('#b_cancelar').show()
  $('#b_eliminar').show()
}
