window.$ = window.jQuery = require('jquery')
const {Auditoria,Usuario} = require('../modelo/modelos.js')//Modelos
const path = require('path')
const util = require('../extra/util')//Utilidades

/**************************/
//Libreria para cargar archivos json
//npm install jsonfile
var jsonfile = require('jsonfile')

/**Para recuperar el funcionario que se logeo*/
const getUsuario = require('electron').remote.getGlobal('getUsuario')
let fechaActual = new Date() //fecha el sistema

function setFechaActual(){
     $('#fecha').val(fechaActual.toISOString().split('T')[0])
}

setFechaActual();

$('#btn_buscar').on('click', function () {
  filtrar()
})

function filtrar() {
   let filtro = $.trim($('#buscador').val())
   let modulo = $('#s_modules').val()
   let fechaIni = $('#fecha').val() + " 00:00"
   let fechaFin = $('#fecha').val() + " 23:59"
   let servicios = []
   let condicionesUsuario = {
      nick: {
         $like: '%' + filtro.toUpperCase() + '%'
      }
   }
   let condicionesAuditoria = {
      createdAt: {
         $between: [fechaIni,fechaFin]
      }
   }
   if(modulo != "todo"){
     condicionesAuditoria.moduloId = modulo
   }
   Auditoria.findAll({
      where: condicionesAuditoria,
      order: [
            ['createdAt', 'ASC'],
      ],
      include: [{
         model: Usuario,
         attributes: ['nick'],
         where: condicionesUsuario
      }]
   }).then(results => {
      results.forEach(function (servicio) {
         servicios.push({
           id:servicio.dataValues.id,
           usuario:servicio.dataValues.usuario.nick,
           createdAt:servicio.dataValues.createdAt.toLocaleDateString('en-GB') + " " + servicio.dataValues.createdAt.toLocaleTimeString('en-GB'),
           accion: servicio.dataValues.accion,
           moduloId: servicio.dataValues.moduloId.toUpperCase(),
         })
      });
      util.buildTable(servicios, 't_auditoria')
   })
}

exports.insertarAuditoria = function(moduloId, accion){
  Auditoria.sync().then(() => {
    return Auditoria.create({
      "moduloId": moduloId,
      "accion": accion,
      "usuarioId": getUsuario().id,
    })
  }).then(function() {})
}

exports.cargarModulos = function(){
  var file = path.join(__dirname, '../extra/modules.json')
  util.buildSelect({
    id:'todo',
    descripcion:'TODO'
  }, 's_modules')
  jsonfile.readFile(file, function(err, result) {
    for (var i = 0; i < result.length; i++) {
      util.buildSelect({
        id:result[i].id,
        descripcion:result[i].id.toUpperCase()
      }, 's_modules')
    }
  })
}
