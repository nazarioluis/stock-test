const Sequelize = require('sequelize')
const sequelize = new Sequelize('postgres://postgres:12345@127.0.0.1:5432/test_stock');

//IsUnique personalizado
require('../extra/sequelizeUnique')(Sequelize)

exports.Rol =  sequelize.define("roles", {
   descripcion: {type: Sequelize.STRING(25),allowNull:false},
   accesos: {type: Sequelize.JSONB,allowNull: false,comment: "Guarda los niveles de acceso en formato JSONB"
   }
},{
   validate: {
      isUnique: sequelize.validateIsUnique(['descripcion'],
         "'Ya existe el rol '+self.descripcion")
   }
})

exports.Usuario = sequelize.define("usuarios",{
   nick:{type: Sequelize.STRING(20),allowNull: false},
   pass:{type: Sequelize.STRING(),allowNull: false},
   estado: {type: Sequelize.STRING(4),allowNull: false,defaultValue: "ACTI",comment: "ACTI=activo, INAC=inactivo"}
},{
   validate: {
      isUnique: sequelize.validateIsUnique(['nick'],
         "'Ya existe el usuario '+self.nick")
   }
})
this.Usuario.belongsTo(this.Rol,{foreignKey: {allowNull: false}});

//Define departamentos
exports.Departamento = sequelize.define('departamento', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripción'}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! Ya existe el departamento '+self.descripcion")
  }
});

//Define ciudades
exports.Ciudad = sequelize.define('ciudad', {
  descripcion: {type: Sequelize.STRING,allowNull: false, comment: 'Descripción'}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
    "'No se pudo realizar la accion! "+
    "Ya existe la ciudad '+self.descripcion+"+"'para el departamento seleccionado'")
  }
});
this.Ciudad.belongsTo(this.Departamento,{onDelete:"restrict"});

// Define Auditoria
exports.Auditoria =  sequelize.define("auditorias", {
   moduloId: {type: Sequelize.STRING(20),allowNull:false,comment: 'Modulo del sistema auditado'},
   accion: {type: Sequelize.STRING(40),allowNull: false,comment: 'Proceso realizado por el Usuario'}
})
this.Auditoria.belongsTo(this.Usuario,{onDelete:"restrict"});

/*
**Sequelize tiene un problema de pluralizacion con la terminacion 'a' ejemplo categoria
**Los terminan en otras vocales o consonantes pluraliza agregando un 's' ejemplo producto -> productos
**Y para las clave si es productos -> productoId y como estaba categoria -> categoriumId puso
**Si termina en 'a' necesariamente se agrega 's' al final para que sea categorias - categoriaId
**Tenes que eliminar la tabla de productos y la de categoria y volve a ejecutar ahi ya va funcionar
**Solo la 's' faltaba
*/
exports.Categoria = sequelize.define("categorias",{
  descripcion: {type: Sequelize.STRING(255),allowNull:false,comment: "Descripción"}
  },{
    validate: {
      isUnique: sequelize.validateIsUnique(['descripcion'],
      "'No se pudo realizar la accion! Ya existe la categoria '+self.descripcion")
    }
});
exports.Seccion = sequelize.define("seccion",{
  descripcion: {type: Sequelize.STRING, allowNull:false, comment:"Seccion"}
},{
  validate: {
    isUnique: sequelize.validateIsUnique(['descripcion'],
  "'No se pudo realizar la accion! Ya existe la seccion '+self.descripcion")
  }
});

exports.Funcionario = sequelize.define("funcionario",{
  nombre: {type: Sequelize.STRING(255), allowNull:false, comment:'Nombre'},
  apellido: {type: Sequelize.STRING(255), allowNull:false, comment:'Apellido'},
  fechanacimiento: {type: Sequelize.DATEONLY, allowNull:false, comment:'Fecha de Nacimiento'},
  documento: {type: Sequelize.STRING(255), allowNull:false, comment:'Documento/C.I'},

},{
  validate: {
      isUnique: sequelize.validateIsUnique(['documento'],
      "'No se pudo realizar la accion! Ya existe el Documento '+self.documento")
    }
});
this.Funcionario.belongsTo(this.Seccion,{onDelete:"restrict"});

exports.Producto = sequelize.define("producto",{
    descripcion: {type: Sequelize.STRING(255),allowNull:false,comment:'Descripción'},
    caracteristica: {type: Sequelize.STRING(255),allowNull:false,comment:'Caracteristica del Produco'},
    preciocompra: {type: Sequelize.DOUBLE,allowNull:false,comment:'Precio de Compra'},
    preciounitario: {type: Sequelize.DOUBLE,allowNull:false,comment:'Precio Unitario del Producto'},
    stockreal: {type: Sequelize.INTEGER, allowNull:false,comment:'Stock del Producto'},
},{
  validate: {
      isUnique: sequelize.validateIsUnique(['descripcion'],
      "'No se pudo realizar la accion! Ya existe el Producto '+self.descripcion")
    }
}
);
this.Producto.belongsTo(this.Categoria,{onDelete:"restrict"});
// OBLIGATORIO AL FINAL
sequelize.sync();
