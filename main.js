const electron = require('electron')
const app = electron.app
const Menu = electron.Menu
const BrowserWindow = electron.BrowserWindow
const jsonfile = require('jsonfile')

app.commandLine.appendSwitch('disable-renderer-backgrounding')

var template = []

const path = require('path')
const url = require('url')

// Keep a global reference of the window object, if you don't, the window will
// be closed automatically when the JavaScript object is garbage collected.
let mainWindow
let dimensions
let usuario
let permisos_especiales = []
let config

function _Menu(label,submenu){
   this.label = label;
   this.submenu = submenu;
}
function recuperarConfig(){
  var file = path.join(__dirname, '../../config.json')
  jsonfile.readFile(file, function(err, result) {
    if(result) config = JSON.parse(JSON.stringify(result))
    else config = {"db_host":"localhost:5432","rp_host":"http://localhost/reportico/reportico/","empresa":""}
    openWindow('login', 50, 60)
  })
}
global.getEmpresa = function(){
  return config.empresa
}
global.getDBHost = function(){
  return config.db_host
}
global.getEspecial = function(){
   return permisos_especiales
}
global.setUsuario = function(o){
   usuario = JSON.parse(o)
}
global.getUsuario = function(){
   return usuario
}
global.crearMenu = function(opciones) {
   var o = _group_by(opciones,'tipo')
   Object.keys(o).forEach(function(key){
      if (key!="especial") {
         var submenuList = []
         for (var i = 0; i < o[key].length; i++) {
            if (o[key][i].tiene_acceso) {
               let id = o[key][i].id
               let w = o[key][i].w
               let h = o[key][i].h
               let maximizar = o[key][i].maximizar
               let uri = o[key][i].url ? o[key][i].url : null;

               var _submenu = new _Menu(o[key][i].opcion,null)
               _submenu.click = function(){
                  if (uri != null) {
                     abrirInforme(uri);
                  } else {
                     openWindow(id, w, h, maximizar);
                  }
               }
               submenuList.push(_submenu)
            }
         }
         if (submenuList.length>0) {
            var _menu = new _Menu(key,submenuList)
            template.push(_menu)
         }
      }else{
         permisos_especiales = o[key]
      }
      var menu = Menu.buildFromTemplate(template)
      Menu.setApplicationMenu(menu)
   })
}
function _group_by(xs, key) {
  return xs.reduce(function(rv, x) {
   (rv[x[key]] = rv[x[key]] || []).push(x);
       return rv;
   }, {});
}

global.createWindow = function() {
   // Create the browser window.
   const screen = electron.screen
   let mainScreen = screen.getPrimaryDisplay();
   dimensions = mainScreen.size;
   mainWindow = new BrowserWindow({
      width: parseInt(dimensions.width * 0.70),
      height: parseInt(dimensions.height * 0.70),
      icon: path.join(__dirname, 'img/glasses.png')
   })
   mainWindow.maximize()
   // and load the index.html of the app.
   mainWindow.loadURL(url.format({
      pathname: path.join(__dirname, 'contenido/index.html'),
      protocol: 'file:',
      slashes: true
   }))

   // Open the DevTools.
  //  mainWindow.webContents.openDevTools()

   // Emitted when the window is closed.
   mainWindow.on('closed', function () {
      // Dereference the window object, usually you would store windows
      // in an array if your app supports multi windows, this is the time
      // when you should delete the corresponding element.
      mainWindow = null
   })
}

// This method will be called when Electron has finished
// initialization and is ready to create browser windows.
// Some APIs can only be used after this event occurs.
app.on('ready', function(){
    recuperarConfig()
})

// Quit when all windows are closed.
app.on('window-all-closed', function () {
   // On OS X it is common for applications and their menu bar
   // to stay active until the user quits explicitly with Cmd + Q
   if (process.platform !== 'darwin') {
      app.quit()
   }
})

app.on('activate', function () {
   // On OS X it's common to re-create a window in the app when the
   // dock icon is clicked and there are no other windows open.
   if (mainWindow === null) {
      createWindow()
   }
})
function abrirInforme(uri){
   uri = config.rp_host + uri;
   electron.shell.openExternal(uri);
}

function openWindow(name, w, h, maximizar) {
  if(!dimensions){
    const screen = electron.screen
    let mainScreen = screen.getPrimaryDisplay();
    dimensions = mainScreen.size;
  }

   // Create the browser window.
   let m_window = new BrowserWindow({
      width: parseInt(dimensions.width * (w / 100)),
      height: parseInt(dimensions.height * (h / 100)),
      icon: path.join(__dirname, 'img/glasses.png')
   })
   m_window.setMenu(null);
   if(maximizar) m_window.maximize()
   // and load the index.html of the app.
   m_window.loadURL(url.format({
      pathname: path.join(__dirname, 'contenido/' + name + '.html'),
      protocol: 'file:',
      slashes: true
   }))

   // Open the DevTools.
    m_window.webContents.openDevTools()
   m_window.on('closed', function () {
      mainWindow = null
   })
}

function cerrar() {
   if (process.platform !== 'darwin') {
      app.quit()
   }
}
