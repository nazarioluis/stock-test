/**************************/
//Generador de tablas dinamico https://www.dynatable.com/
require('../dynatable/jquery.dynatable')
/**************************/

/**************************/
//separador de miles/
require('jquery-number')
/**************************/

/**************************/
//Paquete para convertir formulario de manera automatica
//npm install jquery-serializejson
//https://github.com/marioizquierdo/jquery.serializeJSON
require('jquery-serializejson')
/**************************/

// Para usar con crearPdf
// const  fs  =  require ( 'fs' )
// const {shell} = require ( 'electron' ).remote

function puntosDeMiles(){
  $("input.number").number( true, 0, "."," " )
}
puntosDeMiles()

$.dynatableSetup({
   table: {
      defaultColumnIdStyle: 'underscore'
   },
   features: {
      search: false,
      paginate: false,
      recordCount: false,
      sorting: true
   }
})

let dynatable = Array()

//Medodo que construye la tabla
exports.buildTable = (data, t_name) => {
   if (dynatable[t_name] == null) {
      dynatable[t_name] = $('#' + t_name).dynatable({
        features: {
           sort: false,
        },
         dataset: {
            records: []
         }
      }).data('dynatable')
   }
   if (Array.isArray(data)) {
      dynatable[t_name].settings.dataset.originalRecords = data
   } else {
      dynatable[t_name].settings.dataset.originalRecords.push(data)
   }
   dynatable[t_name].process()
   separadorDeMilesTablaPorClase(t_name)
}

function separadorDeMilesTablaPorClase(t_name){
  let ths = $('#' + t_name + ' th.number')
  if (ths.length>0) {
    let trs = $('#' + t_name + ' tr')
    ths.each(function(){
      let index = $(this).index();
      for (var i = 0; i < trs.length; i++) {
        $(trs[i]).find('td').eq(index).number( true, 0, ",","." )
      }
    });
  }

}

let dynaselect = Array()

function optionWriter(rowIndex, record, columns, cellWriter) {
   var option = '<option value=' + record.id + '>' + record.descripcion + '</option>';
   return option;
}

exports.buildSelect = (data, s_name) => {
   if (dynaselect[s_name] == null) {
      dynaselect[s_name] = $('#' + s_name).dynatable({
        features: {
           sort: false,
        },
         table: {
            bodyRowSelector: 'option'
         },
         dataset: {
            records: []
         },
         writers: {
            _rowWriter: optionWriter
         }
      }).data('dynatable')
   }
   if (Array.isArray(data)) {
      dynaselect[s_name].settings.dataset.originalRecords = data
   } else {
      dynaselect[s_name].settings.dataset.originalRecords.push(data)
   }
   dynaselect[s_name].process()
}

//Cagar datos a un formulario
exports.cargarFormulario = (data, frm) => {
   $.each(data, function (key, value) {
      var ctrl = $('[name=' + key + ']', "#" + frm)
      switch (ctrl.prop("type")) {
         case "radio":
            $('[name=' + key + '][value=' + value + ']', "#" + frm).prop('checked', true);
            break
         case "checkbox":
            ctrl.each(function () {
               $(this).attr("checked", value)
            })
            break
         default:
            ctrl.val(value)
      }
   });
}

//Agrega delay a una Funcion
exports.delay = (function () {
   var timer = 0;
   return function (callback, ms) {
      clearTimeout(timer);
      timer = setTimeout(callback, ms)
   }
})()

exports.mayus = function (f_name) {
   $('#' + f_name + ' :input[type=text]').val(function () {
      return this.value.toUpperCase();
   })
   $('#' + f_name + ' textarea').val(function () {
      return this.value.toUpperCase();
   })
}

exports.llenarConZero = function (val,cant){
  var str = "" + val
  var pad = ""
  for (var i = 0; i < cant; i++) {
    pad = pad + "0"
  }
  var ans = pad.substring(0, pad.length - str.length) + str
  return ans;
}

//Entern navigation
exports.enterNavigation = function () {
   $('input').on("keypress", function (e) {
      /* ENTER PRESSED*/
      if (e.keyCode == 13) {
         var inputs = $(this).parents("form").eq(0)
            .find(":input")
         var idx = inputs.index(this);
         if (idx == inputs.length - 1) {
            inputs[0].select()
         } else {
            if (inputs[idx + 1].type != 'select-one' && inputs[idx + 1].type != 'submit') {
               inputs[idx + 1].select();
            }
            inputs[idx + 1].focus(); //  handles submit buttons
         }
         return false;
      }
   });
}

jQuery.fn.single_double_click = function () {
   let _arguments = arguments;
   return this.each(function () {
      var clicks = 0,
         self = jQuery(this);
      if (typeof _arguments[0] === 'string') {
         jQuery(this).on('click', _arguments[0], function (event) {
            that = jQuery(this)
            clicks++;
            if (clicks == 1) {
               setTimeout(function () {
                  if (clicks == 1) {
                     _arguments[1].call(that, event);
                  } else {
                     _arguments[2].call(that, event);
                  }
                  clicks = 0;
               }, 300);
            }
         })
      } else {
         jQuery(this).click(function (event) {
            clicks++;
            if (clicks == 1) {
               setTimeout(function () {
                  if (clicks == 1) {
                     _arguments[0].call(self, event);
                  } else {
                     _arguments[1].call(self, event);
                  }
                  clicks = 0;
               }, 300);
            }
         });
      }
   });
}


//  * Crea un archivo PDF a partir de una ventana de electron
//  * @param  {String()} file_name   nombre del archivo
//  * @param  {electron.remote.getCurrentWebContents()} webContent Instancia actual de la ventana
//
// exports.crearPdf = function(file_name, webContent){
//    var win = webContent
//    // Usa las opciones de impresión predeterminadas
//    win.printToPDF ({}, ( error , data ) => {
//      if (error) throw error
//      fs.writeFile ( __dirname+'/tmp/'+file_name+'.pdf', data, ( error ) => {
//       if (error) throw error
//       console.log ( 'PDF creado con éxito' )
//       shell.openItem(__dirname+'/tmp/'+file_name+'.pdf')
//     })
//   })
// }
